import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { ElektraService } from './elektra/elektra.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
	
	constructor(
		public elektra: ElektraService
		) {
			
		}
		
		intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
			req = req.clone({
				setHeaders: {
					// 'Content-Type' : 'application/json; charset=utf-8',
					// 'Accept'       : 'application/json',
					// 'Authorization': `Bearer ${this.elektra.token}`,
				},
			});
			return next.handle(req);
		}
		
	}
	