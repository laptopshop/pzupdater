import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ManagerService {

  server : string;
  constructor(
		private http: HttpClient
		) {
      this.server = "http://localhost:3002/api/";
   }
  
  public getGroups(pzId?: number) {
		return this.http.get(this.server + 'categories/' + ((pzId) ? pzId : ''));
  }

}
