import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { LineInterface } from '../../_interfaces/manager';
import { ProductInterface } from '../../_interfaces/pz';

@Injectable({
  providedIn: 'root'
})
export class PzService {

  server : string;
  constructor(
		private http: HttpClient
		) {
      this.server = "http://localhost:3001/api/";
   }
  
  public getProductsByLine(group:LineInterface) {
		return this.http.get(this.server + 'productos?filter[where][Linea]=' + group.pz_id);
  }
  public checkProductByClave(clave:string) {
		return this.http.get(this.server + 'productos?filter[where][Clave]=' + clave);
  }
  
}
