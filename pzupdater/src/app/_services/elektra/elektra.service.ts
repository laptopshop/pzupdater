import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { elektraProductInterface } from '../../_interfaces/elektra';

import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators'; 

@Injectable({
	providedIn: 'root'
})
export class ElektraService {
	
	// private server = 'http://elektrapreprod.mysellercenter.com/sellercenter/api/v1/oauth/token?grant_type=password&username=sistemas@laptopshop.mx&password=123456';
	private server = 'http://elektrapreprod.mysellercenter.com/sellercenter/api/v1/';
	private headers : HttpHeaders = new HttpHeaders();
	
	constructor(
		public http: HttpClient
		) {
		}
		
		public getToken() {
			var headers = new HttpHeaders().set('Authorization',`Basic c2VsbGVyY2VudGVyY2xpZW50aWQ6MTIzNDU2`);
			var req = "oauth/token?grant_type=password&username=sistemas@laptopshop.mx&password=123456";
			return this.http.post(`${this.server}${req}`,null,{ headers: headers }).toPromise().then((credentials)=>{
				this.headers = this.headers.set('Authorization', `Bearer ${credentials['access_token']}`);
			});
		}
		
		public getCategories() {
			return this.http
				.get(this.server+'categories/tree',{ headers: this.headers });
		}
		
		public getSkus(): Observable<elektraProductInterface[]> {
			return this.http
				.get<elektraProductInterface[]>(`${this.server}products`, { headers: this.headers });
		}

		public updateInventoryBySku(sku:string, stock:number): Observable<any> {
			return this.http
				.put(
					`${this.server}sku/inventories/${sku}`,
					{"totalQuantity": stock},
					{ headers: this.headers });
		}
}
