import { TestBed } from '@angular/core/testing';

import { ElektraService } from './elektra.service';

describe('ElektraService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ElektraService = TestBed.get(ElektraService);
    expect(service).toBeTruthy();
  });
});
