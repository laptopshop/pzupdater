import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
	providedIn: 'root'
})
export class MagentoService {
	
	server: any;	
	constructor(
		public http: HttpClient
		) {  
		this.server = {
			0: "http://localhost:3010/api/", // laptopshop
			1: "http://localhost:3011/api/", // cybersport
		};
	}

	
	checkProductEntity(market:number, sku:string) {
		return this.http.get(this.server[market] + 'catalog_product_entities?filter[fields][entity_id]=true&filter[where][sku]=' + sku);
	}
	
	updateCIStockStatus(market:number, entity_id:number, qty:number) {
		return this.http.post(this.server[market]
			+ 'cataloginventory_stock_statuses/update?where={"product_id":' + entity_id + '}',
			{"qty":qty}
		);
	}
		
	updateCIStockItems(market:number, entity_id:number, qty:number) {
		return this.http.post(this.server[market]
			+ 'cataloginventory_stock_items/update?where={"product_id":' + entity_id + '}',
			{"qty":qty}
		);
	}
	
	updateProductEntityDecimal(market:number, entity_id:number, attr:number, value) {
		return this.http.post(this.server[market]
			+ 'catalog_product_entity_decimals/update?where={"entity_id":' + entity_id + ',"attribute_id":' + attr + '}',
			{"value":value}
		);
	}
		
	updateCatalogProductIndexPrice(market:number, entity_id:number, price:number) {
		return this.http.post(this.server[market]
			+ 'catalog_product_index_prices/update?where={"entity_id":' + entity_id + '}',
			{"price":price}
		);
	}
	
}