import { TestBed } from '@angular/core/testing';

import { EnsambladorService } from './ensamblador.service';

describe('EnsambladorService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EnsambladorService = TestBed.get(EnsambladorService);
    expect(service).toBeTruthy();
  });
});
