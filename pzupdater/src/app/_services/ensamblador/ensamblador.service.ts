import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
	providedIn: 'root'
})
export class EnsambladorService {
	
	server: any;
	constructor(
		public http: HttpClient
		) {
			this.server = 'https://ensamblador.herokuapp.com/api/';
		}
		
		public getAllProducts() {
			return this.http.get(this.server + 'Products');
		}
		
		public updateProduct(productId:number, product) {
			return this.http.patch(this.server
				+ 'Products/' + productId,
				product
			);
		}
		
	}
	