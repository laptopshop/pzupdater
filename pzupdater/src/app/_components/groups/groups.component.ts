import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MatTableDataSource } from '@angular/material';

import { ManagerService } from '../../_services/manager/manager.service';
import { LineInterface } from '../../_interfaces/manager';
import { timer } from 'rxjs';

@Component({
	selector: 'app-groups',
	templateUrl: './groups.component.html',
	styleUrls: ['./groups.component.sass']
})
export class GroupsComponent implements OnInit {
	
	displayedColumns: string[] = ['pz_id', 'pz_line', 'pz_revenue'];
	groupsDataSource : MatTableDataSource<LineInterface>;
	
	groups: Array<LineInterface>;
	origin: string = 'ls';
	waitingData = true;
	
	constructor(
		private route: ActivatedRoute,
		private managerService: ManagerService,
		) { }
	
	ngOnInit() {

		this.managerService.getGroups().subscribe((groups:Array<LineInterface>) => {
			this.groups = groups;
			this.waitingData = false;
			this.route.params.subscribe(params => {
				if(params['action'] == "export") this.exportProducts();
				this.setOrigin();
			});
		});

	}
	
	public applyFilter(filterValue: string) {
		this.groupsDataSource.filter = filterValue.trim().toLowerCase();
	}

	public setOrigin(){
		localStorage.setItem('UpdaterOriginId', this.origin);
		switch(this.origin){
			case "ls": this.groupsDataSource = new MatTableDataSource(this.groups.filter((g)=> {return g.magento_market == 0}));
				break;
			case "cs": this.groupsDataSource = new MatTableDataSource(this.groups.filter((g)=> {return g.magento_market == 1}));
				break;
		}
	}

	public exportProducts() {
		timer(10000).subscribe(val => { window.close(); });
		this.groups.forEach((group) => {
			console.log(group);
			
			// if(typeof(group.magento_category) == 'number') window.open("http://localhost:4200/export/" + group.pz_id, "_blank");
			if(typeof(group.magento_category) == 'number') window.open("/pz/export/" + group.pz_id, "_blank");
		});
	}
	
}