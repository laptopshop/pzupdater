import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Usage } from '../../_classes/products/usage'
import { Elektra } from 'src/app/_classes/elektra/elektra';

import { elektraProductInterface } from '../../_interfaces/elektra';

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.sass']
})
export class UpdateComponent implements OnInit {

  public status:string;
	public qtyResults = 0;
	public platform : string;

  constructor(
    private route: ActivatedRoute,
    private productUsage: Usage,
    private elektra: Elektra
		) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.platform = params['platform'];
      this.status = "Procesando..."
      if(this.platform == "ensamblador"){
        this.productUsage.getEnsambladorProducts().then((products: Array<any>) => {
          this.qtyResults = products['length'];
          this.productUsage.updateEnsambladorProducts(products).then((nProductsUpdated)=>{
            if(nProductsUpdated == this.qtyResults) window.close();
            else this.status = 'Productos pendientes de sincronizar. Verifique la consola.';
          });
        })
      } else if(this.platform == "elektra") {
        this.elektra.elektraAuth().then(()=>{
          this.elektra.getElektraSkus().then((skus:Array<string>)=>{
            this.qtyResults = skus.length;
            this.productUsage.updateElektraStocks(skus).then((nProductsUpdated)=>{
              if(nProductsUpdated == this.qtyResults) window.close();
              else this.status = 'Productos pendientes de sincronizar. Verifique la consola.';
            });
          });
        });
      }
		});
  }

}
