import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Usage } from '../../_classes/products/usage'
import { LineInterface } from '../../_interfaces/manager';

@Component({
	selector: 'app-export',
	templateUrl: './export.component.html',
	styleUrls: ['./export.component.sass']
})
export class ExportComponent implements OnInit {

	status:string;
	qtyResults = 0;
	line : string;

	constructor(
		private route: ActivatedRoute,
		private productUsage: Usage
		) { }

	ngOnInit() {
		this.route.params.subscribe(params => {
			this.productUsage.exportCategoryDataByLine(params['group'], false).then((group: LineInterface)=> {
				this.status = "Procesando..."
				console.log('%c' + group.pz_line, 'background: #000; color: #fff');
				if( group['length'] > 0){
					this.qtyResults = group['length'];
					this.line = group.pz_line;
					this.productUsage.updateMagentoProducts().then((nProductsUpdated)=>{
						this.status = 'Actualizados: ' + nProductsUpdated;
						window.close();
					});
				} else window.close();
			});
		});
	}

}
