import { Component, ViewChild, OnInit, ElementRef } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { ActivatedRoute } from '@angular/router';

import { Usage } from '../../_classes/products/usage'

import { ProductInterface } from '../../_interfaces/pz';
import { LineInterface } from '../../_interfaces/manager'

@Component({
	selector: 'app-products',
	templateUrl: './products.component.html',
	styleUrls: ['./products.component.sass']
})
export class ProductsComponent implements OnInit {

	@ViewChild(MatPaginator) paginator: MatPaginator;
	@ViewChild(MatSort) sort: MatSort;
	@ViewChild('componentContainer') container: ElementRef;
	
	productColumns = ['Clave', 'Descrip', 'Existencia','Costo','Precio','Marca'];
	productsDataSource : MatTableDataSource<ProductInterface>;
	tableRows = 20;
	
	qtyResults = 0;
	line : string;
	status : string;
		
	constructor(
		private route: ActivatedRoute,
		private productUsage: Usage
		) { }
	
	ngOnInit() {
		
		this.tableRows = Math.floor((this.container.nativeElement.offsetHeight - 175) / 31);
		this.route.params.subscribe(params => {
			this.productUsage.exportCategoryDataByLine(params['group'], true).then((group:LineInterface)=>{
				this.line = group.pz_line;
				if(group['products'] != undefined) {
					this.qtyResults = group['length'];
					this.productsDataSource = new MatTableDataSource(group['products']);
					this.productsDataSource.paginator = this.paginator;
					this.productsDataSource.sort = this.sort;
				}
			});
		});
	}
	
	//------------------------------------------------------------------------------------------------
	
	public applyFilter(filterValue: string) {
		this.productsDataSource.filter = filterValue.trim().toLowerCase();
	}

	public importProducts() {
		console.log('%c' + this.line, 'background: #000; color: #fff');
		this.status = 'Procesando ...';
		if(this.productsDataSource.data.length>0){
			this.productUsage.updateMagentoProducts().then((nProductsUpdated)=>{
				this.status = 'Actualizados:' + nProductsUpdated;
			});
		}
	}

}