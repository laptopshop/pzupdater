import { Injectable } from '@angular/core';
import { ElektraService } from '../../_services/elektra/elektra.service';
import { elektraProductInterface, elektraSkuInterface } from '../../_interfaces/elektra';

@Injectable({
	providedIn: 'root'
})
export class Elektra {

	constructor(
		private elektraService: ElektraService,
		) { }
		
	public elektraAuth() : Promise<undefined> {
		return new Promise((resolve, reject) => {
			this.elektraService.getToken().then(()=>{
				resolve();
			});
		});
	}

	public getElektraCategories(){
		return new Promise((resolve, reject) => {
			this.elektraService.getCategories().subscribe((response)=>{
				resolve(response);
			});
		});
	}

	public getElektraSkus(){
		return new Promise((resolve, reject) => {
			this.elektraService.getSkus().subscribe((response: Array<elektraProductInterface>)=>{
				var skus = response.map((product: elektraProductInterface) => {
					return product.skus.map((sku: elektraSkuInterface) => {
						return {
							id: sku.id,
							upc: sku.upc,
						}
					});
				});
				resolve(Array.prototype.concat.apply([], skus));
			});
		});
	}

	public updateInventoryBySku(sku:string, stock:number){
		return new Promise((resolve, reject) => {
			this.elektraService.updateInventoryBySku(sku,stock).subscribe((response)=>{
				resolve(response);
			});
		});
	}

}
