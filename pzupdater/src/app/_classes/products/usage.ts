import { Injectable } from '@angular/core';

import { ProductInterface } from '../../_interfaces/pz';
import { LineInterface } from '../../_interfaces/manager';
import { Entity } from '../../_interfaces/magento';

import { PzService } from '../../_services/pz/pz.service';
import { ManagerService } from '../../_services/manager/manager.service';
import { MagentoService } from '../../_services/magento/magento.service';
import { EnsambladorService } from '../../_services/ensamblador/ensamblador.service';
import { ElektraService } from '../../_services/elektra/elektra.service';
import { elektraSkuInterface } from 'src/app/_interfaces/elektra';

export interface categorizedProducts {
	"exist": Array<number>;
	"notExist": Array<any>;
}


@Injectable({
	providedIn: 'root'
})

export class Usage {

	market: number;
	products: Array<ProductInterface>;

	constructor(
		private pzService: PzService,
		private managerService: ManagerService,
		private magentoService: MagentoService,
		private ensambladorService: EnsambladorService,
		private elektraService: ElektraService,
		) { }

	// ------------------------------------------------------------------------------------------------------------

	public exportCategoryDataByLine(lineId:number, retrieveProducts:boolean) {
		return new Promise((resolve, reject) => {
			console.log("3 - Getting Group Data");
			this.managerService.getGroups(lineId).subscribe((group:LineInterface)=>{
				console.log("2 - Getting Products");
				this.market = group.magento_market;
				this.pzService.getProductsByLine(group).subscribe((products:Array<ProductInterface>)=>{
					group['length'] = products.length;
					if(products.length > 0 ){
						this.products = products.map(product => {
							product['Precio'] = Math.ceil((product.Costo * 11600) / (100 - group.pz_revenue)) / 100;
							return product;
						})
						if (retrieveProducts) {
							group['products'] = this.products;
						}
						resolve(group);
					} else resolve(group);
				});
			});
		});
	}

	// ------------------------------------------------------------------------------------------------------------
	
	public asyncForEach = async (array: Array<any>,  callback) => {
		for (let index = 0; index < array.length; index++) {
			await callback(array[index], index, array)
		}
	}

	// ------------------------------------------------------------------------------------------------------------

	public updateMagentoProducts = async (inputProducts?:Array<ProductInterface>) => {

		if(inputProducts) this.products = inputProducts;
		var nProductsUpdated = 0;
		console.log(" SKU");

		return await this.asyncForEach(this.products, async (product:ProductInterface, index) => {
			await new Promise((resolve, reject) => {
				this.magentoService.checkProductEntity(this.market, product.Clave).subscribe(
					(entity: Array<Entity>) => {
						if(entity.length != 0) { // Si el producto existe en el e-commerce
							console.log(" " + product.Clave + " - " + entity[0].entity_id);
							this.updateProductEntities(entity[0].entity_id,product).then(()=>{
								nProductsUpdated ++;
								resolve();
								
							})
						} else resolve();
					},
					error => { reject(error); }
					);
			});
		}).then(()=>{
			console.log('✓ - Actualización Completa ', nProductsUpdated);
			return nProductsUpdated;
		});
	}
	// ------------------------------------------------------------------------------------------------------------

	public updateElektraStocks = async (inputProducts?:Array<string>) => {

		var nProductsUpdated = 0;
		console.log(" SKU");

		return await this.asyncForEach(inputProducts, async (sku:elektraSkuInterface, index) => {
			await new Promise((resolve, reject) => {
				this.pzService.checkProductByClave(sku.upc).subscribe(
					(product: Array<ProductInterface>) => {
						if(product.length != 0) { // Si el producto existe en pz
							console.log(" " + sku.upc + " - " + product[0].Existencia);
							this.elektraService.updateInventoryBySku(sku.id,product[0].Existencia).subscribe((response)=>{
								nProductsUpdated ++;
								resolve(response);
							});
						} else {
							console.log(" / " + sku.upc);
							resolve();
						}
					},
					error => { reject(error); }
					);
			});
		}).then(()=>{
			console.log('✓ - Actualización Completa ', nProductsUpdated);
			return nProductsUpdated;
		});
	}

	//------------------------------------------------------------------------------------------------
	
	public getEnsambladorProducts(){
		return new Promise((resolve, reject) => {
			this.ensambladorService.getAllProducts().subscribe((products:Array<string>)=>{
				resolve(products);
			});
		});
	}

	public updateEnsambladorProducts = async (inputProducts?:Array<any>) => {
		var nProductsUpdated = 0;
		return await this.asyncForEach(inputProducts, async (product, index) => {
			await new Promise((resolve, reject) => {
				this.pzService.checkProductByClave(product['sku']).subscribe(
					(p: Array<ProductInterface>) => {
						if(p.length != 0) { // Si el producto existe en pz
							this.ensambladorService.updateProduct(product['id'],{
									"stock": p[0].Existencia,
									"price": p[0].Costo * 1.03,
								}).subscribe(()=>{
									nProductsUpdated ++;
									resolve();
							})
						} else {
							console.log(product);
							resolve();
						}
					},
					error => { reject(error); }
					);
			});
		}).then(()=>{
			console.log('✓ - Actualización Completa de ' + nProductsUpdated + ' productos');
			return nProductsUpdated;
		});
	}

	//------------------------------------------------------------------------------------------------

	public updateProductEntities(entity_id: number, product: ProductInterface) {

		var _stock = new Promise((resolve, reject) => {
			this.magentoService.updateCIStockStatus(this.market,
				entity_id,
				product.Existencia
			).toPromise().then(
				(entity:Entity) => { resolve(entity);},
				err => { reject(err); }
			);
		});

		var _stockLimit = new Promise((resolve, reject) => {
			this.magentoService.updateCIStockItems(this.market,
				entity_id,
				product.Existencia
			).toPromise().then(
				(entity:Entity) => { resolve(entity);},
				err => { reject(err); }
			);
		});

		return Promise.all([
			_stock,
			_stockLimit,
		]);
		
	}

	//------------------------------------------------------------------------------------------------
	

}
