import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LayoutModule } from '@angular/cdk/layout';

import {
	MatToolbarModule, 
	MatButtonModule, 
	MatSidenavModule, 
	MatIconModule, 
	MatListModule, 
	MatFormFieldModule, 
	MatInputModule,
	MatSelectModule,
	MatTableModule,
	MatPaginatorModule,
	MatSortModule,
} from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { GroupsComponent } from './_components/groups/groups.component';
import { ProductsComponent } from './_components/products/products.component';
import { ExportComponent } from './_components/export/export.component';
import { UpdateComponent } from './_components/update/update.component';

import { AuthInterceptor } from './_services/auth-interceptor';

@NgModule({
	declarations: [
		AppComponent,
		GroupsComponent,
		ProductsComponent,
		ExportComponent,
		UpdateComponent,
	],
	imports: [
		BrowserModule,
		AppRoutingModule,
		HttpClientModule,
		LayoutModule,
		MatToolbarModule,
		MatButtonModule,
		MatSidenavModule,
		MatIconModule,
		MatListModule,
		MatFormFieldModule,
		MatInputModule,
		MatSelectModule,
		MatTableModule,
		MatPaginatorModule,
		MatSortModule,
		BrowserAnimationsModule,	
	],
	providers: [
		{
			provide: HTTP_INTERCEPTORS,
			useClass: AuthInterceptor,
			multi: true 
		}
	],
	bootstrap: [AppComponent]
})
export class AppModule { }