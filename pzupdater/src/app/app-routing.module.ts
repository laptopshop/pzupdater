import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GroupsComponent } from './_components/groups/groups.component';
import { ProductsComponent } from './_components/products/products.component';
import { ExportComponent } from './_components/export/export.component';
import { UpdateComponent } from './_components/update/update.component';

const routes: Routes = [
  {
    path: 'groups',
    component: GroupsComponent,
    pathMatch: 'full'
  },
  {
    path: 'groups/:action',
    component: GroupsComponent,
    pathMatch: 'full'
  },
  {
    path: 'group/:group',
    component: ProductsComponent,
    pathMatch: 'full'
  },
  {
    path: 'export/:group',
    component: ExportComponent,
    pathMatch: 'full'
  },
  {
    path: 'update/:platform',
    component: UpdateComponent,
    pathMatch: 'full'
  },
  {
    path: '**',
    redirectTo: 'groups'
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
