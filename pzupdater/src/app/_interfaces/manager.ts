
export interface LineInterface {
    pz_id:number;
    pz_line:string;
    pz_revenue:number;
    magento_category:number;
    magento_market:number;
}