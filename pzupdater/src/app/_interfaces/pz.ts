export interface ProductInterface {
    "Clave": string,
    "Descrip": string,
    "Linea": number,
    "Existencia": number,
    "Costo": number,
    "DescripDetallada": string,
    "Marca": string
}
