export interface elektraProductInterface {
    "skus": Array<elektraSkuInterface>
}
export interface elektraSkuInterface {
    "id": string
    "upc": string
}
